module Invmed
  module Parsers
    require 'parsers/base'
    class NDA < Parsers::Base

      def domain
        'www.nda.ru'
      end

      private

      def parse_page(url)
        parse_products_on_page(url).flatten
      end

      def parse_products_on_page(page_url)
        download_page(page_url).css('div.nda-postcontent div.list-item').map do |product_div|
          product_page_url     = parse_product_url(product_div)
          product_detailed_div = download_page(product_page_url).css('div.itemView')
          if product_detailed_div.empty?
            parse_products_on_page(product_page_url)
          else
            parse_product(product_detailed_div, product_page_url)
          end
        end
      end

      def parse_product(product_div, product_url)
        Models::Product.new(
          category_url:   @page_path,
          product_url:    product_url,
          name:           parse_product_name(product_div),
          description:    parse_product_description(product_div),
          attributes:     parse_product_attributes(product_div)
        )
      end

      def parse_product_url(product_div)
        url = product_div.css('div.subCategory a').attribute('href').value.strip
        full_page_url(URI.escape(url))
      end

      def parse_product_description(product_div)
        product_div.css('div.itemBody p').reduce('') do |full_text, para|
          return full_text if para.class == 'Nokogiri::XML::Element'
          full_text + para.text
        end
      end

      def parse_product_attributes(product_div)
        table = product_div.css('div.catFullText table tr')

        table_header      = table.shift
        attributes_keys   = table_header.css('td').map(&:text)

        table_rows   = table
        variants = table_rows.map do |row|
          values = row.css('td').map(&:text)
          attributes_keys.zip(values).to_h
        end

        {
          'variants'  => variants,
          'image_url' => parse_product_image_url(product_div)
        }
      end

      def parse_product_image_url(product_div)
        image_url = product_div.css('div.itemImageBlock img').attribute('src')&.value
        if image_url.nil?
          image_url = product_div.css('div.sigProGalleriaWhitePlaceholder img').attribute('src')&.value
        end
        image_url ? full_page_url(image_url) : nil
      end

      def parse_product_name(product_div)
        product_div.css('h1.itemTitle').text.strip
      end
    end
  end
end