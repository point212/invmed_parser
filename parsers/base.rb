module Invmed
  module Parsers
    class Base
      require 'nokogiri'
      require 'open-uri'
      require 'models/product'

      def domain
        ''
      end

      attr_reader :page_path
      attr_reader :products

      def initialize(page_path)
        @page_path = page_path
      end

      def call
        @products ||= parse_category(@page_path)
      end

      private

      def get_pages_list
        [full_page_url(@page_path)]
      end

      private

      def parse_category(category_url)
        get_pages_list.reduce([]) do |products, url|
          products + parse_page(url)
        end
      end

      def download_page(page_url)
        page_file = open(full_page_url(page_url), 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0.')
        Nokogiri::HTML(page_file)
      end

      def parse_page(url)
      end

      def parse_product(product_div)
      end

      def full_page_url(url)
        return url if url.match(/^(http|https):\/\//)
        "http://#{domain}#{url}"
      end
    end
  end
end