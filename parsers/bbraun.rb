module Invmed
  module Parsers
    require 'parsers/base'
    class BBraun < Parsers::Base

      def domain
        'www.bbraun.ru'
      end

      private

      def parse_category(url)
        product_pages_urls = download_page(encode_url(url)).css('div._product-tile a').map do |url_elem|
          url_elem.attribute('href').text.strip
        end

        product_pages_urls.reduce([]) do |products, page_url|
          products << parse_page(page_url)
        end
      end

      def parse_page(url)
        product_page_url = full_page_url(url)
        pp product_page_url
        page = download_page(product_page_url)
        parse_product(page, product_page_url)
      end

      def parse_product(product_div, product_url)
        Models::Product.new(
          category_url:   @page_path,
          product_url:    product_url,
          name:           parse_product_name(product_div),
          description:    parse_product_description(product_div),
          attributes:     parse_product_attributes(product_div)
        )
      end

      def parse_product_description(product_div)
        product_div.css('._product-base ._product-text').xpath('.//text() | text()').map(&:text).map(&:strip).join("\n")
      end

      def parse_product_attributes(product_div)
        {
          'image_url' => parse_product_image_url(product_div)
        }
      end

      def parse_product_image_url(product_div)
        image_url = product_div.css('picture._image__picture img').attribute('srcset')&.value
        image_url ? full_page_url(image_url) : nil
      end

      def parse_product_name(product_div)
        product_div.css('span._hero__headline').text.strip
      end

      def encode_url(url)
        (base, query) = url.match(/(.*)(\?.*)/)[1,2]
        query_encoded = URI.escape(query, '/:')
        "#{base}#{query_encoded}"
      end
    end
  end
end