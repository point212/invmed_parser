module Invmed
  module Parsers
    require 'parsers/base'
    class Sagemed < Parsers::Base
      MAX_PRODUCTS_NUMBER = 10000

      def domain
        'sagemed.ru'
      end

     private

      def get_pages_list
        page_with_all_products = "#{@page_path}/results,1-#{MAX_PRODUCTS_NUMBER}"
        [page_with_all_products]
      end

      def parse_page(page_url)
        download_page(page_url).css('section#main-row-2-col-2 > [id^="product"]').map { |product_div| parse_product(product_div) }
      end

      def parse_product(product_div)
        Models::Product.new(
          category_url:   @page_path,
          product_url:    parse_product_url(product_div),
          name:           parse_product_name(product_div),
          description:    parse_product_description(product_div),
          attributes:     parse_product_attributes(product_div)
        )
      end

      def parse_product_url(product_div)
        full_page_url(product_div.css('h3 a').attribute('href').value.strip)
      end

      def parse_product_description(product_div)
        product_div.css('div.product-descriptions span').text.strip
      end

      def parse_product_attributes(product_div)
        table = product_div.css('table.product-details-table tr')

        table_header = table[0]
        table_values = table[1]

        additional_description = table[2]

        attributes_keys   = table_header.css('td').map(&:text)
        attributes_values = table_values.css('td').map(&:text)

        attributes_keys[0]   = 'image_url'
        attributes_values[0] = full_page_url(table_values.css('td#product-details-table-img > a').attribute('href').value)

        attributes                           = attributes_keys.zip(attributes_values).to_h
        attributes['additional_description'] = additional_description&.text&.strip
        attributes
      end

      def parse_product_name(product_div)
        product_div.css('h3 a').text.gsub(/^[0-9\-]+\s/, '')
      end
    end
  end
end