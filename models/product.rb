module Invmed
  module Models
    class Product
      require 'digest/md5'

      def initialize(attributes)
        @attributes   = attributes
      end

      def to_a
        [category_url, name, description, image_url]
      end

      def to_h
        @attributes
      end

      def image_url
        @attributes[:attributes]['image_url']
      end

      def image_filename
        return nil if image_url.nil?
        name = Digest::MD5.hexdigest(image_url)
        ext = image_url.match(/(.*)\.(.*?)$/)[2]
        filename = "#{name}.#{ext}"
      end

      def method_missing(method, *args)
        @attributes[method]
      end
    end
  end
end