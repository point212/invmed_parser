module Invmed
  module Dumpers
    class ProductsCSV
      require 'csv'

      def initialize(file_name)
        @file_name = file_name
      end

      def call(products)
        CSV.open(@file_name, 'w+') do |file|
          #csv header for model Products
          file << %w[
              category_url
              product_url
              product_name
              product_description
              product_image_url
              product_image_filename
          ]
          products.each do |product|
            file << [
              product.category_url,
              product.product_url,
              product.name,
              product.description,
              product.image_url,
              product.image_filename
            ]
          end
        end
      end
    end
  end
end
