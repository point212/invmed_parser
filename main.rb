require './boot'
require 'watir'
require 'pry'

# Initalize the Browser
browser = ::Watir::Browser.new :chrome, headless: true, options: { args: ['disable-gpu', 'no-sandbox', 'disable-dev-shm-usage'], binary: "/usr/bin/chromium" }

# Navigate to Page
browser.goto 'bbraun.ru'

browser.execute_script("return jQuery('._quick-access__item').size()")
binding.pry

#pp browser.divs

browser.quit