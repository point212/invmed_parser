require './boot'

module Invmed
  class App
    require 'parsers/sagemed'
    require 'parsers/nda'
    require 'parsers/bbraun'
    require 'dumpers/csv_dumper'

    def initialize
      # @category_urls = %w(
      #   http://www.sagemed.ru/produkty/anestesiology/воздуховоды
      #   http://www.sagemed.ru/produkty/anestesiology/аэрозольная-терапия
      #   http://www.sagemed.ru/produkty/anestesiology/респираторная-поддержка
      #   http://www.sagemed.ru/produkty/anestesiology/маски-резервные-мешки-аксессуары
      #   http://www.sagemed.ru/produkty/anestesiology/трахеостомия
      #   http://www.sagemed.ru/produkty/anestesiology/эндобронхиальные-трубки
      #   http://www.sagemed.ru/produkty/anestesiology/эндотрахеальные-трубки
      #   http://www.sagemed.ru/produkty/surgery/hem-o-lok
      #   http://www.sagemed.ru/produkty/surgery/зонды-и-дренажи
      #   http://www.sagemed.ru/produkty/urology/уретральные-катетеры-фолея
      #   http://www.sagemed.ru/produkty/urology/безбаллонные-уретральные-катетеры
      #   http://www.sagemed.ru/produkty/urology/надлобковая-цистостомия
      #   http://www.sagemed.ru/produkty/urology/мочеприемники
      #   http://www.sagemed.ru/produkty/urology/мочеточниковые-катетеры
      #   http://www.sagemed.ru/produkty/urology/мочеточниковые-стенты-jpg
      #   http://www.sagemed.ru/produkty/urology/нефростомия
      #   http://www.sagemed.ru/produkty/urology/экстракторы-камней
      #   http://www.sagemed.ru/produkty/urology/катетеры-для-уродинамических-исследований
      #   http://www.sagemed.ru/produkty/urology/изделия-специального-назначения
      #   http://www.nda.ru/catalog/suture.html
      #   http://www.nda.ru/catalog/surgical-staplers.html
      #   http://www.nda.ru/catalog/laparoscopic-stepler.html
      #   http://www.nda.ru/catalog/skin-staplers.html
      #   http://www.nda.ru/catalog/klipatory-klipapplikatory.html
      #   http://www.nda.ru/catalog/trocar.html
      #   http://www.nda.ru/catalog/hernia.html
      #   http://www.nda.ru/catalog/electrosurgery.html
      #   http://www.nda.ru/catalog/laparoscopic-instruments.html
      # )

      @category_urls = %w(
        http://www.bbraun.ru/ru/products-and-therapies/product-catalog.products.html?category=/content/brands/bbraun/ru/website/ru/products-and-therapies/product-catalog/infusion-therapy/drugs-admixture-tools/jcr:content
        http://www.bbraun.ru/ru/products-and-therapies/product-catalog.products.html?category=/content/brands/bbraun/ru/website/ru/products-and-therapies/product-catalog/infusion-therapy/administration-sets/jcr:content
        http://www.bbraun.ru/ru/products-and-therapies/product-catalog.products.html?category=/content/brands/bbraun/ru/website/ru/products-and-therapies/product-catalog/infusion-therapy/infusion-transfusionaccessories/jcr:content
        http://www.bbraun.ru/ru/products-and-therapies/product-catalog.products.html?category=/content/brands/bbraun/ru/website/ru/products-and-therapies/product-catalog/infusion-therapy/venipuncture-andinjection/jcr:content
      )

      @domain_parsers = {
        'sagemed.ru' => Parsers::Sagemed,
        'www.sagemed.ru' => Parsers::Sagemed,
        'nda.ru' => Parsers::NDA,
        'www.nda.ru' => Parsers::NDA,
        'bbraun.ru' => Parsers::BBraun,
        'www.bbraun.ru' => Parsers::BBraun,
      }
    end

    def call
      @category_urls.each do |url|
        ap "Processing url #{url}"

        filename = url2filename(url)
        products = parser(url).call
        products.each { |p| save_image(p) }
        Dumpers::ProductsCSV.new(filename).call(products)
      end
    end

    def save_image(product)
      return if product.image_filename.nil?
      return if File.exist?(product.image_filename)

      File.open(product.image_filename,'w') do |file|
        file << open(product.image_url).read
      end
    end

    def url2filename(url)
      page_name = url.gsub(/http:\/\//,'').gsub(/\//,'_').gsub(/\.html$/,'')
      "#{page_name}.csv"
    end

    def parser(url)
      domain = url.match(/(http|https):\/\/(.*?)\//)[2]
      url_encoded = URI.escape(url)
      @domain_parsers[domain].new(url_encoded)
    end
  end
end

Invmed::App.new.call


