USER = $(shell id -u):$(shell id -g)
USER_PARAM=--user ${USER}
APP_VOLUME_PARAM=-v `pwd`/.:/app
CONTAINER_NAME=invmed_parser/ruby25
DOCKER_RUN=docker run -it ${USER_PARAM} ${APP_VOLUME_PARAM} ${APP_PORT_PARAM} ${CONTAINER_NAME}
DOCKER_COMPOSE_RUN=docker-compose run ${USER_PARAM} app

build-image:
	docker build \
	    -t ${CONTAINER_NAME} \
	    -f Dockerfile.development \
	    .

sh:
	${DOCKER_COMPOSE_RUN} /bin/bash

console:
	${DOCKER_COMPOSE_RUN} rails console

root-sh:
	docker run -it ${APP_VOLUME_PARAM} ${CONTAINER_NAME} /bin/bash

up:
	docker-compose up

down:
	docker-compose down

test:
	${DOCKER_COMPOSE_RUN} bundle exec rspec -f d spec/

lint:
	${DOCKER_COMPOSE_RUN} rubocop -C false -c .rubocop.yml

